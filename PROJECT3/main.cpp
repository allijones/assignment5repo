#include <iostream>
#include "MyVector-proj3.h"

using namespace std;


int main()
{
    MyVector<int> aString;

    //using a vector to put numbers in this array
    for(int i = 0; i < 11; i++)
    {
        aString.pushBack(i);
    }

    cout << "This is a vector that holds the values 0-10:" << endl;
    //print all the elements in the vector
    for(int i = 0; i < aString.getSize(); i++)
    {
        cout << aString[i] << " ";
    }
    cout << endl;

    //print the size, capacity, front element, and last element
    cout << "The size is: " << aString.getSize() << endl;
    cout << "The capacity is: " << aString.getCapacity() << endl;
    cout << "The front element is: " << aString.front() << endl;
    cout << "The last element is: " << aString.back() << endl;

    //adding a 2 to the beginning of the array
    aString.pushFront(2);

    //adding a 90 to the end of the array
    aString.pushBack(90);

    cout << endl;
    cout << "A 2 has been added to the beginning of the vector"
         << " and a 90 has been added to the end." << endl;
    for(int i = 0; i < aString.getSize(); i++)
    {
        cout << aString[i] << " ";
    }
    cout << endl;

    cout << "The size is: " << aString.getSize() << endl;
    cout << "The capacity is: " << aString.getCapacity() << endl;
    cout << "The front element is: " << aString.front() << endl;
    cout << "The last element is: " << aString.back() << endl;

    cout << endl;
    cout << "The last element, 90, was removed from the vector..." << endl;
    aString.popBack(90);
    cout << "The front element is: " << aString.front() << endl;
    cout << "The last element is: " << aString.back() << endl;

    cout << endl;
    cout << "The first element, 2, was removed from the vector..." << endl;
    cout << "90 has also been previously removed..." << endl;
    aString.popFront(2);
    cout << "The front element is: " << aString.front() << endl;
    cout << "The last element is: " << aString.back() << endl;
    return 0;
}
