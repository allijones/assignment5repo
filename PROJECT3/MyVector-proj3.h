#ifndef MYVECTOR_PROJ3_H_INCLUDED
#define MYVECTOR_PROJ3_H_INCLUDED
#include <sstream>
#include <iostream>
#include "ContainerIfc-proj3.h"

using namespace std;

template <class T>
class MyVector : public ContainerIfc<T>
{
private:
    T *data;
    int length;
    int capacity;
    void grow()
    {
        T* temp = this->data;
        this->capacity += 5;
        this->data = new T[this->capacity];
        for(int i = 0; i < this->length; i++)
        {
            this->data[i] = temp[i];
        }
        delete[]temp;
    }
    void shiftRight()
    {
        for(int i = this->length; i > 0; i--)
        {
            data[i] = data[i - 1];
        }
    }
    void shiftLeft()
    {
        for(int i = 0; i < this->length; i++)
        {
            i = data[i + 1];
        }
        this->length++;
    }

public:
    MyVector()
    {
        this->capacity = 10;
        this->length = 0;
        this->data = new T[this->capacity];
    }

    ~MyVector()
    {
        delete [] this->data;
    }

    MyVector(const MyVector& that)
    {
        this->length = that.length;
        this->capacity = that.capacity;
        this->data = new T[this->capacity];
        for(int i = 0; i < this->length; i++)
        {
            this->data[i] = that.data[i];
        }
    }


    MyVector<T>& operator=(const MyVector& that)
    {
        if(this != &that);
        {

            delete[]this->data;
            this->length = that.length;
            this->capacity = that.capacity;
            delete[]this->data;
            this->data = new T[this->capacity];
            for(int i = 0; i < this->length; i++)
            {
                this->data[i] = that.data[i];
            }
        }
        return *this;
    }


    MyVector<T>& pushFront(T val)
    {
        if(this->length >= this->capacity)
        {
            grow();
        }
        shiftRight();
        this->data[0] = val;
        this->length++;
    }

    MyVector<T>& pushBack(T val)
    {
        if(this->length >= this->capacity)
        {
            grow();
        }
        this->data[this->length] = val;
        this->length++;
    }

    MyVector<T>& popFront(T val)
    {
        data[0] = 0;
        shiftLeft();
        this->length--;
        return *this;

    }

    MyVector<T>& popBack(T val)
    {
        data[this->length] = data[this->length--];
        //this->length--;
        return *this;
    }


    T front()
    {
        return this->data[0];
    }

    T back()
    {
        return this->data[this->length - 1];
    }


    T& operator[](int index)
    {
        return this->data[index];
    }

    int getSize()
    {
        return this->length;
    }

    int getCapacity()
    {
        return this->capacity;
    }

    T getData()
    {
        for(int i = 0; i < this->length; i++)
        {
            return data[i];
        }
    }

    bool isEmpty()
    {
        if(this->length == 0)
        {
            return true;
        }
    }

};



#endif // MYVECTOR-PROJ3_H_INCLUDED
