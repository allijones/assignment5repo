Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
	
	Logger.log is different from the log to console because the log has 
	extra information about the exceptions, like �FINER�.

2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	
	This line comes from logger.log.

3.  What does Assertions.assertThrows do?
	
It causes the program to crash if the execution of the supplied executable throws an exception of the expectedType and returns the exception.  It is different from assertTrue in that assertTrue causes the program to crash if the Boolean value it is testing is false.  AssertThrow tests an executable.

4.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it?
    2.  Why do we need to override constructors?
    3.  Why we did not override other Exception methods?	

5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

The static {} is a try-catch block that executes before the main method.  It makes sure that the logger is configured properly.

6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)



7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

The issue was that timeNow was being assigned a value in the try-catch block.  It needs to be assigned to the current system time before the try-catch block, or else it will stay as null in some cases.  After I moved the line that assigns timeNow before the try statement, the program passed all three jUnit tests.

8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)



9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

	

10.  Make a printScreen of your eclipse Maven test run, with console

		

11.  What category of Exceptions is TimerException and what is NullPointerException



12.  Push the updated/fixed source code to your own repository.

	Done!
